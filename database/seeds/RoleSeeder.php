<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert(
        ['id' => 1, 'name' => 'user', 'created_at' => '2022-03-12 09:02:19', 'updated_at' => '2022-03-12 09:02:19']
      );

      DB::table('roles')->insert(
        ['id' => 2, 'name' => 'admin', 'created_at' => '2022-03-12 09:02:19', 'updated_at' => '2022-03-12 09:02:19']
      );
    }
}
