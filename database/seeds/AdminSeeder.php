<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name' => 'admin', 
        'email' => 'admin@admin.com',
        'password' => bcrypt('password'),
        'birthday' => '1945-08-17',
        'role_id' => 2,
        'created_at' => '2022-03-12 09:02:19', 
        'updated_at' => '2022-03-12 09:02:19'
      ]);
    }
}
