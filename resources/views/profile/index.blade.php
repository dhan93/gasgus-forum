@extends('layouts.main')
@section('title')
Manage User
@endsection
@section('content')

@push('style')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">    
@endpush
@push('script')
  <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
  <script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
  </script>
@endpush

<a href="{{route('profile.create')}}" class="btn btn-primary mb-3">Add User</a>

<table id="example" class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Age</th>
        <th scope="col">Role</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($users as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->name}}</td>
            <td>{{\Carbon\Carbon::parse($item->birthday)->age}}</td>
            <td>{{$item->role->name}}</td>
            <td>
                <form class="mt-2" action="{{route('profile.destroy', $item->id)}}" method="POST">
                  <a href="{{route('profile.show', $item->id)}}" class="btn btn-primary btn-sm">detail</a>
                  <a href="{{route('profile.edit', $item->id)}}" class="btn btn-warning btn-sm">Edit</a>
                  @csrf
                  @method('delete')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
        </tr>
      @empty
        <h1>Data Tidak Ditemukan</h1>
      @endforelse
    </tbody>
  </table>

  @endsection