@extends('layouts.main')
@section('title')
Edit Profile
@endsection

@section('content')
<form method="POST" action="{{route('profile.update', $user->id)}}">
    @method('PUT')
    @csrf
    {{-- name --}}
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="name" class="form-control" value="{{$user->name}}">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    {{-- role --}}
    @if (auth()->user()->role_id == 2)
    <div class="form-group">
      <label>Role</label>
      <select class="form-control" id="role" name="role_id">
        @foreach ($roles as $item)
          @if ($user->role_id == $item->id)
            <option value="{{$item->id}}" selected>{{$item->name}}</option>        
          @else
            <option value="{{$item->id}}">{{$item->name}}</option>    
          @endif
        @endforeach        
      </select>
    </div>
    @error('role_id')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    @endif
    {{-- email --}}
    <div class="form-group">
      <label>Email</label>
      <input type="email" name="email" class="form-control" value="{{$user->email}}">
    </div>
    @error('email')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    {{-- birthday --}}
    <div class="form-group">
      <label>Tanggal Lahir</label>
      <input id="datepicker" type="date" name="birthday" class="form-control" value="{{$user->birthday}}">
    </div>
    @error('birthday')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    {{-- address --}}
    <div class="form-group">
      <label>Alamat</label>
      <input type="text" name="address" class="form-control" value="{{$user->address}}">
    </div>
    @error('address')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    {{-- bio --}}
    <div class="form-group">
      <label>Bio</label>
      <textarea class="form-control" name="bio" id="bio" rows="3">{{$user->bio}}</textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    {{-- pass --}}
    <div class="form-group">
      <label>Password</label>
      <input type="password" name="password" class="form-control">
    </div>
    @error('password')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    {{-- pass confirm --}}
    <div class="form-group">
      <label>Konfirmasi Password</label>
      <input type="password" name="password_confirmation" class="form-control">
    </div>
    @error('password_confirmation')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection