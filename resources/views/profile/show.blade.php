@extends('layouts.main')
@section('title')
Profile of {{$user->name}}
@endsection

@section('content')
  <table>
    <tr>
      <td>Nama</td>
      <td>: {{$user->name}}</td>
    </tr>
    <tr>
      <td>Email</td>
      <td>: {{$user->email}}</td>
    </tr>
    <tr>
      <td>Tanggal Lahir</td>
      <td>: {{$user->birthday}}</td>
    </tr>
    <tr>
      <td>Alamat</td>
      <td>: {{$user->address}}</td>
    </tr>
    <tr>
      <td>Bio</td>
      <td>: {{$user->bio}}</td>
    </tr>
  </table>
  <hr>
  <h3>Threads by {{$user->name}}</h3>
  <ul class="list-group">
    @forelse ($topics as $item)
      <li class="list-group-item">
        <a href="{{route('topic.show', $item->id)}}">
          <h2>{{$item->title}}</h2>
        </a>
        by <a href="{{route('profile.show', $item->user->id)}}">{{$item->user->name}}</a> |
        <span>category: <a href="{{route('category.show', $item->category->id)}}">{{$item->category->name}}</a></span> |
        <span class="timestamp">{{$item->created_at->diffForHumans()}}</span>
      </li>
    @empty
      <p>Data Not Found</p>
    @endforelse
  </ul>
@endsection