@extends('layouts.main')
@section('title')
Topic
@endsection

@section('card-tools')
  <a href="{{route('topic.create')}}" class="btn btn-primary mb-3">Add</a>
@endsection

@section('content')

  <ul class="list-group">
    @forelse ($topic as $item)
      <li class="list-group-item">
        <a href="{{route('topic.show', $item->id)}}">
          <h2>{{$item->title}}</h2>
        </a>
        by <a href="{{route('profile.show', $item->user->id)}}">{{$item->user->name}}</a> |
        <span>category: <a href="{{route('category.show', $item->category->id)}}">{{$item->category->name}}</a></span> |
        <span class="timestamp">{{$item->created_at->diffForHumans()}}</span>
      </li>
    @empty
      <h1>Data Not Found</h1>
    @endforelse
  </ul>
  @endsection