@extends('layouts.main')
@section('title')
Edit Topic
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/w87cwhy6axadle00918qysz0z2pvnwkmp6hdplhqmdggqks9/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea'
  });
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
  $(document).ready(function() {
  $('.js-example-basic-single').select2();
});
</script>
@endpush

@push('style')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
<form method="POST" action="{{route('topic.update', $topic->id)}}" enctype="multipart/form-data">
  @method('PUT')
  @csrf
    <div class="form-group">
      <label>Category</label><br>
      <select id="category" name="category_id" class="form-control js-example-basic-single" style="width: 100%">
        @foreach ($categories as $item)
          @if ($topic->category_id == $item->id)
            <option value="{{$item->id}}" selected>{{$item->name}}</option>        
          @else
            <option value="{{$item->id}}">{{$item->name}}</option>    
          @endif
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Title</label>
      <input type="text" name="title" value="{{$topic->title}}" class="form-control">
    </div>
    @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div>
      <label>Content</label>
      <textarea name="content" value="" class="form-control">{{$topic->content}}</textarea>
    </div>
    <div class="form-group">
      <label>Image</label>
      <input type="file" name="image" value="{{$topic->image}}" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection