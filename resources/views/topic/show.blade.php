@extends('layouts.main')
@section('title')
{{$topic->title}}
@endsection

@section('card-tools')
      @if (auth()->user()->id == $topic->user_id)
        <form class="mt-2" action="/topic/{{$topic->id}}" method="POST">
          <a href="/topic/{{$topic->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
          @csrf
          @method('delete')
          <input type="submit" value="Delete" class="btn btn-danger btn-sm">
        </form>
      @endif
@endsection

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <p class="text-muted">
          {{$topic->created_at->diffForHumans()}} by 
          <a href="{{route('profile.show', $topic->user->id)}}">{{$topic->user->name}}</a> under category 
          <a href="{{route('category.show', $topic->category->id)}}">{{$topic->category->name}}</a>
        </p>
    </div>
    <div class="panel-body">
        <div class="d-flex justify-content-center">
          <img src="{{asset('gambar/'.$topic->image)}}" alt="featured image">
        </div>
        
        <p class="lead" >{!!$topic->content!!}</p>
        <hr>
        <div class="btn-group">
            <button id="btn-main-comment" class="btn btn-outline-secondary btn-sm"><i class="lnr lnr-bubble"></i>Add Comment</button>
        </div> 
        <form action="{{route('comment.store')}}" method="POST" id="main-comment" style="margin-top:10px;display:none">
            @csrf
            <input type="hidden" name="topic_id" value="{{$topic->id}}">
            <input type="hidden" name="parent" value="0">
            <textarea class="form-control" name="comment"></textarea>
            <input type="submit" class="btn btn-primary mt-2" value="Send">
        </form><br><br>
        <h4>Comments</h4>
            <ul class="list-unstyled activity-list">
                @foreach ($topic->comment()->where('parent', 0)->orderBy('created_at', 'desc')->get() as $comment)
                <li class="border rounded p-3 my-1">
                    <p><b>{{$comment->user->name}}</b><br>
                    {{$comment->comment}}<br><span class="timestamp">{{$comment->created_at->diffForHumans()}}
                    </span>
                    @if (auth()->user()->id == $comment->user_id)
                      <form class="d-inline" action="{{route('comment.destroy', $comment->id)}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="{{route('comment.edit', $comment->id)}}">Edit</a> | 
                        <input type="submit" value="Delete" class="btn btn-link p-0 d-inline">
                      </form>
                    @endif
                    </p>
                    <form action="{{route('comment.store')}}" method="POST">
                        @csrf
                        <input type="hidden" name="topic_id" value="{{$topic->id}}">
                        <input type="hidden" name="parent" value="{{$comment->id}}">
                        <input type="text" name="comment" class="form-control">
                        <input type="submit" class="btn btn-primary btn-xs mt-2" value="Send">
                    </form>
                    <br>
                        @foreach($comment->childs()->orderBy('created_at', 'desc')->get() as $child)
                            <p style="padding-left: 2em"><b>{{$child->user->name}}</b><br>
                            {{$child->comment}}<br><span class="timestamp">{{$child->created_at->diffForHumans()}}
                            </span>
                            @if (auth()->user()->id == $child->user->id)
                            <span class="mt-2" action="" method="POST">
                            <span>| <a href="{{route('comment.edit', $child->id)}}">Edit</a> | <a href="/topic/{{$topic->id}}/{{$child->id}}/delete">Delete</a></span>
                            @endif
                            </p>
                        @endforeach
                </li>    
                @endforeach
          </ul>
    </div>
</div>

@endsection

@push('script')
    <script>
        $(document).ready(function(){
            $('#btn-main-comment').click(function(){
                $('#main-comment').toggle('slide');
            });
        });
    </script>
@endpush