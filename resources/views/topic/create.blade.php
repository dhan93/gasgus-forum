@extends('layouts.main')
@section('title')
Add Topic
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/w87cwhy6axadle00918qysz0z2pvnwkmp6hdplhqmdggqks9/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea'
  });
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
  // In your Javascript (external .js resource or <script> tag)
  $(document).ready(function() {
  $('.js-example-basic-single').select2();
});
</script>
@endpush

@push('style')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')
<form method="POST" action="/topic" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Category</label><br>
      <select name="category_id" class="form-control js-example-basic-single" style="width: 100%" id="">
        <option value="">-- Category --</option>
        @foreach ($category as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Title</label>
      <input type="text" name="title" class="form-control">
    </div>
    @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div>
      <label>Content</label>
      <textarea name="content" class="form-control"></textarea>
    </div>
    <div class="form-group">
      <label>Image</label>
      <input type="file" name="image" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection