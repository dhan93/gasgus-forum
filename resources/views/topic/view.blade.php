@extends('layouts.main')
@section('title')
Topic
@endsection

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">{{$topic->title}}</h3>
        <p class="panel-subtitle">by {{$topic->user->name}}. {{$topic->created_at->diffForHumans()}}</p>
    </div>
    <div class="panel-body">
        {{$topic->content}}
        @if (auth()->user()->id == $topic->user_id)
        <form class="mt-2" action="/topic/{{$topic->id}}" method="POST">
          <a href="/topic/{{$topic->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
          @csrf
          @method('delete')
          <input type="submit" value="Delete" class="btn btn-danger btn-sm">
        </form>
        @endif
        <hr>
        <div class="btn-group">
            <button id="btn-main-comment" class="btn btn-default"><i class="lnr lnr-bubble"></i>Comment</button>
        </div> 
        <form action="" method="POST" id="main-comment" style="margin-top:10px;display:none">
            @csrf
            <input type="hidden" name="topic_id" value="{{$topic->id}}">
            <input type="hidden" name="parent" value="0">
            <textarea class="form-control" name="comment"></textarea>
            <input type="submit" class="btn btn-primary" value="Send">
        </form><br><br>
        <h4>Comments</h4>
            <ul class="list-unstyled activity-list">
                @foreach ($topic->comment()->where('parent', 0)->orderBy('created_at', 'desc')->get() as $comment)
                <li>
                    <p><a href="#">{{$comment->user->name}}</a><br>
                    {{$comment->comment}}<br><span class="timestamp">{{$comment->created_at->diffForHumans()}}
                    </span></p>
                    <form action="" method="POST">
                        @csrf
                        <input type="hidden" name="topic_id" value="{{$topic->id}}">
                        <input type="hidden" name="parent" value="{{$comment->id}}">
                        <input type="text" name="comment" class="form-control">
                        <input type="submit" class="btn btn-primary btn-xs" value="Send">
                    </form>
                    <br>
                        @foreach($comment->childs()->orderBy('created_at', 'desc')->get() as $child)
                            <p style="padding-left: 2em"><a href="#">{{$child->user->name}}</a><br>
                            {{$child->comment}}<br><span class="timestamp">{{$child->created_at->diffForHumans()}}
                            </span></p>
                        @endforeach
                </li>    
                @endforeach
          </ul>
    </div>
</div>

@endsection

@push('script')
    <script>
        $(document).ready(function(){
            $('#btn-main-comment').click(function(){
                $('#main-comment').toggle('slide');
            });
        });
    </script>
@endpush