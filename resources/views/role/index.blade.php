@extends('layouts.main')
@section('title')
Halaman Role
@endsection
@section('content')

<a href="/role/create" class="btn btn-primary mb-3">Add</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">No</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($role as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->name}}</td>
            <td>
                <form class="mt-2" action="/role/{{$item->id}}" method="POST">
                  <a href="/role/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  @csrf
                  @method('delete')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
              </td>
        </tr>
    @empty
        <h1>Data Tidak Ditemukan</h1>
    @endforelse
    </tbody>
  </table>

  @endsection