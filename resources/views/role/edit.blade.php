@extends('layouts.main')
@section('title')
Halaman Edit Role
@endsection

@section('content')
<form method="POST" action="/role/{{$role->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Role Name</label>
      <input type="text" name="name" value="{{$role->name}}" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection