@extends('layouts.main')
@section('title')
Halaman Tambah Role
@endsection

@section('content')
<form method="POST" action="/role">
    @csrf
    <div class="form-group">
      <label>Role Name</label>
      <input type="text" name="name" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection