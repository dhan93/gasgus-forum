@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid pt-5">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card bg-transparant">
                <div class="card-header"><center><a href="/topic"><h4 style="color:blue;padding:20px;border:3px solid black";a:hover{text-decoration:none; color:#f00;}>Click here to Forum</h4></a><center>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
