@extends('layouts.main')
@section('title')
Category
@endsection

@section('card-tools')
  @if (auth()->user()->role_id == 2)
    <a href="{{route('category.create')}}" class="btn btn-primary mb-3">Add Category</a>       
  @endif
@endsection

@section('content')
<ul class="list-group">
  @forelse ($category as $key => $item)
    <li class="list-group-item">
      <a href="{{route('category.show', $item->id)}}">
        <h2>{{$item->name}}</h2>
      </a>
      <p>{{$item->topic_count}} threads</p>
    </li>
  @empty
      <h1>Data Not Found</h1>
  @endforelse
</ul>
@endsection
