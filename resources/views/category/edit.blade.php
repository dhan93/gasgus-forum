@extends('layouts.main')
@section('title')
Edit Category
@endsection

@section('content')
<form method="POST" action="/category/{{$category->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Category Name</label>
      <input type="text" name="name" value="{{$category->name}}" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection