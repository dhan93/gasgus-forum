@extends('layouts.main')
@section('title')
Add Category
@endsection

@section('content')
<form method="POST" action="/category">
    @csrf
    <div class="form-group">
      <label>Category Name</label>
      <input type="text" name="name" class="form-control">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection