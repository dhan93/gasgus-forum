@extends('layouts.main')
@section('title')
Edit Comment
@endsection

@section('content')
<form action="{{route('comment.update', $comment->id)}}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Comment</label>
    <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$comment->comment}}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Save</button>
</form>
@endsection