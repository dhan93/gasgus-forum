<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $fillable = ["user_id", "topic_id", "comment", "parent"];

    public function user(){
        return $this->belongsTo(User::class);

    }

    public function topic(){
        return $this->belongsTo(Topic::class);

    }

    public function childs(){
        return $this->hasMany(Comment::class, 'parent');
    }
} 
