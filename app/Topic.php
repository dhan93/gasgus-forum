<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table = "topics";
    protected $fillable = ["title", "content", "image", "category_id", "user_id"];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function category(){
      return $this->belongsTo(Category::class);
    }

    public function comment(){
        return $this->hasMany(Comment::class);
    }

    // public function category(){
    //     return $this->hasMany(Category::class);
    // }

}
