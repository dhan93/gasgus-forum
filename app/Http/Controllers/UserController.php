<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Topic;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if (auth()->user()->role_id == 2) {
        $users = User::with('role')
          ->get();
        // return $users;
        return view('profile.index', compact('users'));
      }
      return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if (auth()->user()->role_id == 2) {
        $roles = Role::select('name', 'id')
          ->get();
        return view('profile.create', compact('roles'));
      }
      return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $roles = Role::select('id')->get()->pluck('id');

      Validator::make($request->all(), [
        'name' => ['required', 'string', 'max:255'],
        'birthday' => ['required', 'date'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'bio' => ['text'],
        'address' => ['string', 'max:200'],
        'role_id' => [
            'required',
            Rule::in($roles),
        ],
        'password' => ['required', 'string', 'min:8', 'confirmed'],
      ]);

      $user = User::create([
        'name' => $request['name'],
        'birthday' => $request['birthday'],
        'email' => $request['email'],
        'bio' => $request['bio'],
        'address' => $request['address'],
        'role_id' => $request['role_id'],
        'password' => bcrypt($request['password']),
      ]);

      return redirect(route('profile.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $user = User::find($id);
      $topics = Topic::where('user_id', '=', $id)
        ->get();
      return view('profile.show', compact('user', 'topics'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $roles = Role::select('name', 'id')
          ->get();

      if (auth()->id() == $id || auth()->user()->role_id == 2) {
        $user = User::find($id);
        return view('profile.edit', compact('user', 'roles'));
      }
      return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $roles = Role::select('id')->get()->pluck('id');

      // $request['role_id'] ?? 1;

      Validator::make($request->all(), [
        'name' => ['required', 'string', 'max:255'],
        'birthday' => ['required', 'date'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'bio' => ['text'],
        'address' => ['string', 'max:200'],
        'role_id' => [
            Rule::in($roles),
        ],
        'password' => ['string', 'min:8', 'confirmed'],
      ]);

      $user = User::find($id);
      $user->name = $request['name'];
      $user->birthday = $request['birthday'];
      $user->email = $request['email'];
      $user->bio = $request['bio'];
      $user->address = $request['address'];

      if ($request['role_id'] != '') {
        $user->role_id = $request['role_id'];
      }

      if ($request['password'] != '') {
        $user->password = bcrypt($request['password']);
      }      

      $user->save();

      return redirect(route('profile.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      if (auth()->user()->role_id == 2) {
        $user = User::find($id);
        $user->delete();

        return redirect(route('profile.index'));
      }
      return back();
    }
}
