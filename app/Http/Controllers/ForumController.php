<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Category;
use App\Comment;
use App\Topic;
use App\User;

use Illuminate\Support\Facades\DB;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topic = Topic::orderBy('created_at', 'desc')->paginate(10);
        
        return view('topic.index', compact('topic'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = DB::table('categories')->get();
        return view('topic.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'category_id' => 'required',
        ]);
            
        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('gambar'), $imageName);

        $topic = new Topic;

        $topic->title = $request->title;
        $topic->content = $request->content;
        $topic->image = $imageName;
        $topic->category_id = $request->category_id;
        $topic->user_id = auth()->id(); // yang ini ya.. bisa juga auth()->user()->bio atau auth()->user()->address dst.
        $topic->save();

        // return $topic->id;
        //return redirect(route('topic.show', $topic->id));
        return redirect('/topic');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $topic = Topic::find($id);
      // return $topic;
      return view('topic.show', compact('topic'));
    }

    public function view(Topic $topic)
    {
        return view('topic.view', compact('topic'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::select('name', 'id')->get();

        $topic = Topic::find($id);
        return view('topic.edit', compact('topic', 'categories'));
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categories = Category::select('id')->get()->pluck('id');

      Validator::make($request->all(), [
        'title' => 'required',
        'content' => 'required',
        'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        'category_id' => ['required',
            Rule::in($categories),
        ],
      ]);

      $imageName = time().'.'.$request->image->extension();
      $request->image->move(public_path('gambar'), $imageName);

      $topic = Topic::find($id);
      $topic->title = $request->title;
        $topic->content = $request->content;
        $topic->image = $imageName;
        $topic->category_id = $request->category_id;
        $topic->user_id = auth()->id();
        $topic->save();

      return redirect('topic');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::find($id);
        $topic->delete();

        return redirect('topic');
    }
}
