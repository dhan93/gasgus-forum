<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'ForumController@index');

Auth::routes();

Route::get('/home', 'ForumController@index')->name('home');

Route::middleware(['auth'])->group(function () {
  Route::resource('/category', 'CategoryController');
  Route::resource('/role', 'RoleController');
  Route::resource('/topic', 'ForumController');
  Route::resource('/profile', 'UserController');
  Route::resource('/comment', 'CommentController');


  //Read Topic
  // Route::get('/topic', 'ForumController@index');
  Route::get('/topic/{topic}/view', 'ForumController@view');

  //Update
  Route::get('/topic/{topic_id}/edit', 'ForumController@edit');
  Route::put('/topic/{topic_id}', 'ForumController@update');

  //Create Comment
  Route::post('/topic/{topic}/view', 'CommentController@store');
});




// //CRUD Category
// //Create
// Route::get('/category/create', 'CategoryController@create');
// Route::post('category', 'CategoryController@store');

// //Read
// Route::get('/category', 'CategoryController@index');

// //Update
// Route::get('/category/{category_id}/edit', 'CategoryController@edit');
// Route::put('category/{category_id}', 'CategoryController@update');

// //Delete
// Route::delete('category/{category_id}', 'CategoryController@destroy');

// //CRUD Role
// //Create
// Route::get('/role/create', 'RoleController@create');
// Route::post('role', 'RoleController@store');

// //Read
// Route::get('/role', 'RoleController@index');

// //Update
// Route::get('/role/{role_id}/edit', 'RoleController@edit');
// Route::put('role/{role_id}', 'RoleController@update');

// //Delete
// Route::delete('role/{role_id}', 'RoleController@destroy');