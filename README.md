## About GasGus Forum

Dibuat sebagai pemenuhan tugas project 1 dari kelas Full Stack Web Dev PKS Digital School Batch 2.
Anggota tim 4 dari grup Telegram 2 terdiri atas:
Tujuan : Agar dapat menerapkan fitur blade serta laravel ui ke dalam project

- [Dwi Nurtanto](https://gitlab.com/dwinurtanto).
- [Ramadhan](https://gitlab.com/dhan93).
- [Dicky Syahputra](https://gitlab.com/AarickRushy).

Project ini bertemakan FORUM DISKUSI

Demo applikasi dapat diakses di [https://project1-gasgus.herokuapp.com/](https://project1-gasgus.herokuapp.com/)

Video demo dapat diakses di: [Google Drive](https://drive.google.com/file/d/1v236clLujP1Cz8WA_4tw227fQJO5xwqt/view?usp=sharing)
